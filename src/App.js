import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Phong.css';
import './thuan.css';
import Header from './component/Header';
import Home from './page/Home';
import SignUp from './page/SignUp';
import Footer from './component/Footer';
import Preferences from './page/Preferences';
import MealPlan from './page/MealPlan';
import AccountSetup from './page/AccountSetUp';
import { BrowserRouter as Router, Route } from "react-router-dom";


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      logo :{src: "images/pure_logo.svg", alt: "logo"},
      user :{isLogin: false},
      banner:{img:{src:"images/pure_banner.jpg"}}
    };
  }
  render() {
    return (
      <Router>
        <div className="wrapper App">
          <Header attr = {this.state}  />
          <Route exact path="/" component={Home} />
          <Route path="/signup" component={SignUp} />
          <Route path="/preferences" component={Preferences} />
          <Route path="/mealPlan" component={MealPlan} />
          <Route path="/AccountSetup" component={AccountSetup} />
          <Footer/>
        </div>
      </Router>
    );
  }
}


export default App;
