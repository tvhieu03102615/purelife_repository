import React, {Component} from 'react';

class Description extends Component{
  render(){
    return(
      <section className="description section-padding" id="section-signup-2">
      <div className="container">
        <h1 className="text-center">All about <span className="name-pet">Nala </span></h1>
        <form action="">
          <div className="paragrap">
            <Sex/>&nbsp;is a 
            <Categories/>&nbsp;born in 
            <Date/>&nbsp;, and her ideal <sup>* </sup>&nbsp;weight is 
            <Price/>&nbsp;pounds. 
          </div>
          <div className="paragrap"><span className="des-name">Pet's </span>currently eats
            <Food/>&nbsp;and 
            <Fixed/>&nbsp;.
          </div>
          <div className="paragrap">Overall<span className="des-name">&nbsp;Pet's is</span>
            <Lazy/>&nbsp;.
          </div>
        </form>
      </div>
      </section>
    );
  }
}

class Sex extends Component{
  render(){
    return(
      <select id="sex" name="">
        <option value="She">She</option>
        <option value="He">He</option>
      </select>
    );
  }
}

class Categories extends Component{
  render(){
    return(
      <select id="categories" name="">
        <option value="Greate Dane">Greate Dane </option>
        <option value="Greate Dane">Greate Dane </option>
        <option value="Greate Dane">Greate Dane </option>
        <option value="Greate Dane">Greate Dane </option>
      </select>
    );
  }
}

class Date extends Component{
  render(){
      return(<input id="date" name="date" type="text" placeholder="date"/>
    );
  }
}

class Price extends Component{
  render(){
    return(
      <select id="price" name=""> 
        <option value="100">100</option>
        <option value="100">100</option>
        <option value="100">100</option>
        <option value="100">100</option>
        <option value="100">100</option>
      </select>
    );
  }
}

class Food extends Component{
  render(){
    return(
      <select id="food" name="">
        <option value="She">wet food</option>
        <option value="He">He</option>
      </select>
    );
  }
}

class Fixed extends Component{
  render(){
    return(
      <select id="fixed" name="">
        <option value="Greate Dane">has been fixed </option>
        <option value="Greate Dane">has been fixed </option>
        <option value="Greate Dane">has been fixed </option>
        <option value="Greate Dane">has been fixed </option>
      </select>
    );
  }
}

class Lazy extends Component{
  render(){
    return(
      <select id="lazy" name="">
        <option value="She">a little lazy</option>
        <option value="He">a little lazy</option>
      </select>
    );
  }
}

export default Description 