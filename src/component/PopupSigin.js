import React, { Component } from 'react';
import axios from 'axios';


class PopupSigin extends Component{
  constructor(props){
    super(props);
    this.state ={
      dataLogin: {}
    }
    this.login = this.login.bind(this);
  }

  login(event){
    event.preventDefault();
    var form = event.target;
    var user = {
      email: form.email.value,
      password: form.password.value
    };
    console.log(user);
    axios.post(form.action, { user })
      .then(res => {
        alert('success');
      }).catch(err  => {
        alert('error');
      });
  }

  render(){
    return(
      <div className="popup-sigin popup-site disable">
        <div className="popup-sigin__child">
          <form id="form-login" action="https://pureliferaw.lemonstand.com/login" onSubmit={this.login}>
            <h3 className="title-popup text-center">Log in</h3>
            <label htmlFor="">Email</label>
            <input type="email" name="email"/><br/><br/>
            <label htmlFor="">Password</label>
            <input type="password" name="password"/><br/><br/>
            <input className="btn-default btn-res btn-sigin" type="submit" value="Log in"/>
          </form>
        </div>
      </div>
    );
  }
}

export default PopupSigin;