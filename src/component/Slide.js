import React, { Component } from 'react';

class Slide extends Component{
  constructor(props){
    super(props)
    this.state ={
      listItem:[
        {
          src: "images/pure_slide_1.jpg",
        },
        {
          src: "images/pure_slide_2.png",
        },
        {
          src: "images/pure_slide_1.jpg",
        },
        {
          src: "images/pure_slide_2.png",
        },
        {
          src: "images/pure_slide_1.jpg",
        },
        {
          src: "images/pure_slide_2.png",
        },
        {
          src: "images/pure_slide_1.jpg",
        }
      ]
    }
  }
  render(){
    return(
      <section className="slide" id="section-4">
        <div className="slide__owl owl-theme owl-carousel">
          {this.state.listItem.map((item, index)=>
            <SlideItem key ={index} src={item.src} />
          )}
        </div>
        <p className="text-center">&#64; p u r e l i f e r a w</p>
      </section>
    );
  }
}

class SlideItem extends Component{
  render(){
    return(
      <div className="item"><img src={this.props.src} alt="pure_slide"/></div>
    );
  }
}

export default Slide;