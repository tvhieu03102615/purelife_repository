import React, {Component} from 'react';

class PopupRegister extends Component{
  render(){
    return(
      <div className="popup-register popup-site disable">
        <div className="popup-register__child">
          <form id="form-register" action="">
            <h3 className="title-popup text-center">Register</h3>
            <label htmlFor="">Name</label>
            <input type="text"/><br/><br/>
            <label htmlFor="">Email</label>
            <input type="email"/><br/><br/>
            <label htmlFor="">Password</label>
            <input type="passwork"/><br/><br/>
            <label htmlFor="">Confirm Password</label>
            <input type="passwork"/><br/><br/>
            <input className="btn-default btn-res btn-resgister" type="submit" value="Register"/>
          </form>
        </div>
      </div>
    );
  }
}

export default PopupRegister;