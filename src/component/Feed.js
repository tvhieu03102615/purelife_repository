import React, { Component } from 'react';

class Feed extends Component{
  render(){
    return(
      <section className="feed section-padding" id="section-3">
        <div className="container">
          <div className="feed__elem">
            <h2 className="title-section text-center">feed instinctually</h2>
            <div className="feed__content">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <a className="btn-default btn-white btn-feed" href="#"><span>Learn More</span></a>
              <p className="note">FREE shipping to select states. 
                <a href="#"><u>View Map</u></a>
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Feed;