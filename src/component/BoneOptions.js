import React , {Component} from 'react';

class BoneOptions extends Component{
  render(){
    return(
      <section className="section-padding bone-options">
        <div className="container">
          <h1 className="text-center"> <span className="name-pet">Nala's</span>&nbsp;preferences</h1>
          <h2>Bone options</h2>
          <h3>Our food has the option of ground bone in the food, or on the side. Please choose one.</h3>
          <form action="">
            <div className="bone-options__side">
              <input type="radio" name="bone-options"/>
              <label htmlFor="">Bone on the side</label>
            </div>
            <div className="bone-options__into">
              <input type="radio" name="bone-options"/>
              <label htmlFor="">Bone ground into the food</label>
            </div>
          </form>
        </div>
      </section>
    );
  }
}

class Allergies extends Component{
  render(){
    return(
      <section className="section-padding allergies" >
        <div className="container">
          <h2>Allergies:</h2>
          <h3>Does <span className="name-pet">Nala  </span>&nbsp;have any allergies or dietary restrictions to any of the following?</h3>
          <form action="">
            <input type="text"/>
          </form>
        </div>
      </section>
    );
  }
}

export { BoneOptions,  Allergies};