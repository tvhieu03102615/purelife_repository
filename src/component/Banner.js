import React, { Component } from 'react';
class Banner extends Component{
  // constructor(props){
  //   super(props)
  // }
  render(){
    return(
      <section className="banner full-height" id="section-1">
        <div className="banner__elem text-center">
          <div className="container">
            <BannerHeadImg src={this.props.attr.banner.img.src}/>
            <BannerHeadP/>
            <Search/>
          </div>
        </div>
      </section>
    );
  }
}

class BannerHeadImg extends Component{
  render(){
    return(
        <img className="banner__img" src={this.props.src} alt={this.props.alt}/>
    );
  }
}

class BannerHeadP extends Component{
  render(){
    return(
      <p>high-quality meals starting at &#36;60 per month</p>
    );
  }
}

class Search extends Component{
  render(){
    return(
      <div className="search">
        <form id="form-seachh" action="">
          <h1><span>My</span>
            <input type="text" placeholder="Pet's"/><span>&nbsp;name is	</span>
            <input type="text"/>&nbsp;<a className="btn-default btn-white btn-fetch" href="#"><span>Fetch!</span></a>
          </h1>
        </form>
      </div>
    );
  }
}

export default Banner;