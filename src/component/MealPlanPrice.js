import React, { Component } from 'react';
import { Link } from "react-router-dom";

class MealPlanPrice extends Component{
  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e){
    this.props.onChangeStatus(0);
  }
  render(){
    return(
      <section className={"section-padding meal-plan-st1--price " + (this.props.status===1?"":"disable") }>
        <div className="container">
          <h2> Nala's order total<span className="order-price">125.00</span></h2>
          <div className="fleft">
            <a className="btn-default btn-res" href="#" onClick={this.handleChange}><span>Change your options below</span></a>
          </div>
          <div className="fright">
            <Link className="btn-default btn-res btn-meal-next" to="/AccountSetup"><span>Check out</span></Link>
          </div>
          <div className="clear">     </div>
        </div>
      </section>
    );
  }
}

class MealPlanCalculate extends Component{
  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e){
    this.props.onChangeStatus(1);
  }
  render(){
    return(
      <section className={"meal-plan-st1--btn text-center section-padding " + (this.props.status===0?"":"disable") }>
        <div className="container">
          <a className="btn-default btn-res" href="#" onClick={this.handleChange}><span>Calculate your order total</span></a>
        </div>
      </section>
    );
  }
}

class MealPlanOption extends Component{
  constructor(props){
    super(props);
    this.state={
      status: true,
      head: "how much food does Nala need?",
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleClick2 = this.handleClick2.bind(this);
  }

  handleClick(){
    this.setState({
      status : true
    });
  }

  handleClick2(){
    this.setState({
      status : false
    });
  }
  render(){
    return(
      <section className={"section-padding option text-center option-st1-"+ this.props.index + (this.props.stt===1?" opacity":"")}>
        <div className="container">
          <h2>{this.props.head?this.props.head:this.state.head}</h2>
          <p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, id.</p>
          <form action="">
            <div className="row">
              <div className="col-md-6">
                <div className={"option-st1-"+ this.props.index +"--1"}> 
                <div className={(this.state.status?"active": "") + " option-st1-"+this.props.index+"__child"}  onClick={this.handleClick} >
                    <h3>{this.props.title1}</h3>
                    <p>{this.props.content1}</p>
                    <input type="radio" name={"option-st1-"+this.props.index+".1"} checked={this.state.status}/>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className={"option-st1-"+ this.props.index +"--1"}> 
                  <div className={(this.state.status?"": "active") +" option-st1-"+this.props.index+"__child"} onClick={this.handleClick2}>
                    <h3>{this.props.title2}</h3>
                    <p>{this.props.content2}</p>
                    <input type="radio" name={"option-st1-"+this.props.index+".1"} checked={!this.state.status}/>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    );
  }
}

export  { MealPlanPrice, MealPlanCalculate, MealPlanOption};