import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Header extends Component{
  render(){
    return(
      <header className="header" id="header">
        <div className="header__elem">
          <Logo src={this.props.attr.logo.src} alt={this.props.attr.logo.alt} />
          <User isLogin= {this.props.attr.user.isLogin}/>
          <Nav isLogin ={this.props.attr.user.isLogin} />
          <div className="clear"></div>
        </div>
      </header>
    );
  }
}

class Logo extends Component{
  render(){
    return(
      <div className="logo-site">
        <Link to="/">
          <img src={this.props.src} alt={this.props.alt}/>      
        </Link>
      </div>
    );
  }
}

class User extends Component{

  render(){
    return(
      <div className={this.props.isLogin?"disable":"" + " action-user-site"}>
        <form id="action-user" action=""> <a className="btn-login btn-default" href="#" ><span>Login</span></a><a className="btn-register btn-default btn-white" href="#"><i className="fas fa-paw"> </i><span>&nbsp;New Customer  </span></a>
          <div className="hide"><i className="fas fa-user"></i></div>
        </form>
      </div>
    );
  }
}

class Nav extends Component{
  render(){
    return(
      <nav className={this.props.isLogin?"disable":"" +" main_menu col-right"} id="main_menu">
        <ul className="list-inline">
          <li>
            {/* <Link to="/">Home</Link> */}
            <a href="#">Why Raw?</a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Header