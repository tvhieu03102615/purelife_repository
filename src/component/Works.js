import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Works extends Component{
  render(){
    return(
      <section className="works section-padding" id="section-5">
        <div className="container">
          <div className="works__elem"> 
            <h2 className="title-section title-red">how it works</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
            <div className="works__col">
              <div className="row">
                <WorkItem/>
                <WorkItem/>
                <WorkItem/>
                <WorkItem/>
              </div>
            </div><Link className="btn-default btn-res btn-work" to="/signup"><span>Sign Up Now!</span></Link>
          </div>
        </div>
      </section>
    );
  }
}

class WorkItem extends Component{
  render(){
    return(
      <div className="col-md-3">
        <div className="works__item">
          <div className="works__img"><img src="images/pure_words_1.jpg" alt="pure_words_1"/></div>
          <div className="works__text"> 
            <h3>Lorem, ipsum dolor.</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Works