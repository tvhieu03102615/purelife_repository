import React, {Component} from 'react'

class Looks extends Component{
  render(){
    return(
      <section className="section-padding looks" id="section-signup-2">
        <div className="container">
          <form action="">
            <div className="row text-center">
              <LooksDemo />
              <LooksDemo />
              <LooksDemo />
              <LooksDemo />
            </div>
          </form>
        </div>
      </section>
    );
  }
}

class LooksDemo extends Component{
  render(){
    return(
      <div className="col-md-3">
        <div className="looks__demo--img"><img src="http://via.placeholder.com/180x115" alt="via.placeholder.com"/></div>
        <div className="looks__demo--check-box">
          <input type="radio" name="locks"/><br/>
          <label htmlFor="">too Skinny</label>
        </div>
      </div>
    );
  }
}

export default Looks 