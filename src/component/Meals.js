import React, { Component } from 'react';

class Meals extends Component{
  constructor(props){
    super(props)
    this.state ={
      item:[
        {
          src: "http://via.placeholder.com/86x86",
          alt: "product",
          head: "All Natural",
          p: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus architecto ipsam dolorum blanditiis inventore eos. Voluptates nisi nobis dolor molestiae?"
        },
      ],
      img_lage:{
        src: "",
        alt: ""
      }
    }
  }
  render(){
    return(
        <section className="meals section-padding" id="section-2">
          <div className="container">
            <div className="meals__elem">
              <h2 className="title-section text-center">pre-measured raw meals</h2>
              <div className="row">
                <div className="col-md-8">
                  <div className="meals__content">
                    <MealsItem/>
                    <MealsItem/>
                    <MealsItem/>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="meals__img-right"><img src="http://via.placeholder.com/315x315" alt="via.placeholder"/></div>
                </div>
                <div className="clear">         </div>
              </div>
            </div>
          </div>
        </section>
    );
  }
}


class MealsItem extends Component{
  render(){
    return(
      <div className="meals__item">
        <div className="row"> 
          <div className="col-20 fleft">
            <div className="meals__img"><img src="http://via.placeholder.com/86x86" alt="iso"/></div>
          </div>
          <div className="col-80 fleft">
            <div className="meals__text">
              <h3>All Natural</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus architecto ipsam dolorum blanditiis inventore eos. Voluptates nisi nobis dolor molestiae?</p>
            </div>
          </div>
          <div className="clear"></div>
        </div>
      </div>
    );
  }
}

export default Meals;