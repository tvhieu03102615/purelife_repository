import React, { Component } from 'react';


class Progress extends Component{
  constructor(props){
    super(props);
    this.state = props.attr;
  }
  render(){
    return(
      <section className="progress section-padding">
        <div className="container">
          <div className="progress-bar">
            <div className="col-50 fleft">
              <div className={"process " + (this.state.index>0?"active":"")} id="pcs-1"></div>
            </div>
            <div className="col-50 fleft">
              <div className={"process " + (this.state.index>1?"active":"")} id="pcs-2"></div>
            </div>
            <div className="clear">  </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Progress;