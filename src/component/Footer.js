import React, { Component } from 'react';

class Footer extends Component{
  render(){
    return(
      <footer className="footer" id="footer">
        <div className="footer__top">
          <div className="container">
            <div className="footer__left">
              <div className="row">
                <FooterText />
                <FooterText />
                <FooterText />
                <FooterSocial/>
                <div className="clear"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__bot">
          <div className="container">
            <p>&copy; PureLifeRaw 2018-2019</p>
          </div>
        </div>
      </footer>
    );
  }
}

class FooterText extends Component{
  render(){
    return(
      <div className="col-md-3">
        <div className="footer__elem">
          <h3 className="head">Headline</h3>
          <ul className="list-foot">
            <li><a href="#">Why Raw?</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">FAQ</a></li>
          </ul>
        </div>
      </div>
    );
  }
}

class FooterSocial extends Component{
  render(){
    return(
      <div className="col-md-3">         
        <div className="footer__elem">
          <div className="social col-right">
            <div className="footer__logo"><img src="images/pure_logo.svg" alt="pure_logo"/></div>
            <div className="footer__link">
              <ul className="list-inline">
                <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i className="fab fa-twitter"></i></a></li>
                <li><a href="#"><i className="fab fa-instagram"></i></a></li>
                <li><a href="#"><i className="fab fa-pinterest-p"></i></a></li>
              </ul>
            </div>
          </div>
          <div className="clear">         </div>
        </div>
      </div>
    );
  }
}

export default Footer;