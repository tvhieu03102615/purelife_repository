import React, { Component } from 'react';
import Banner from '../component/Banner';
import Meals from '../component/Meals';
import Feed from '../component/Feed';
import Slide from '../component/Slide';
import Works from '../component/Works';
import PopupSigin from '../component/PopupSigin';
import PopupRegister from '../component/PopupRegister';

class Home extends Component{
  constructor(props){
    super(props);
    this.state = {
      logo :{src: "images/pure_logo.svg", alt: "logo"},
      user :{isLogin: false},
      banner:{img:{src:"images/pure_banner.jpg"}}
    };
  }
  render() {
    return (
      <div  id="content">
        <PopupSigin/>
        <PopupRegister/>
        <Banner attr = {this.state} />
        <Meals />
        <Feed />
        <Slide />
        <Works />
      </div>
    );
  }
}

export default Home;