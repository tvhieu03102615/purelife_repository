import React, { Component } from 'react';
import Progress from '../component/Progress';
import Description from '../component/Description';
import Looks from '../component/Looks';
import { Link } from "react-router-dom";

class SignUp extends Component{  
  constructor(props){
    super(props);
    this.state = {
      logo :{src: "images/pure_logo.svg", alt: "logo"},
      user :{isLogin: false},
      banner:{img:{src:"images/pure_banner.jpg"}},
      process: {
        index: 0,
      },
    };
  }

  render(){
    return(
      <div id="sign-up">
        <Progress attr={this.state.process} />
        <Description/>
        <Looks/>
        <section className="section-padding sign-up-end" id="section-signup-2">
          <div className="container">
            <Link className="btn-default btn-res btn-signup-next" to="/preferences"><span>Next</span></Link>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>
          </div>
        </section>
      </div>
    );
  }
}

export default SignUp;