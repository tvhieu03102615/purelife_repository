import React , {Component} from 'react';
import Progress from '../component/Progress';
import {BoneOptions, Allergies} from '../component/BoneOptions';

import { Link } from "react-router-dom";

class Preferences extends Component{
  constructor(props){
    super(props);
    this.state = {
      logo :{src: "images/pure_logo.svg", alt: "logo"},
      user :{isLogin: false},
      banner:{img:{src:"images/pure_banner.jpg"}},
      process: {
        index: 1,
      },
    };
  }
  render(){
    return(
      <div id="preferences">
        <Progress attr={this.state.process}/>  
        <BoneOptions/>
        <Allergies/>
        <section className="section-padding" id="section-pref-3">
          <div className="container">
            <Link className="btn-default btn-res btn-preferences-back" to="/SignUp"><span>Go back</span></Link>
            <Link className="btn-default btn-res btn-preferences-next" to="/MealPlan"><span>Next step</span></Link>
          </div>
        </section>
      </div>
    );
  }
}

export default Preferences;