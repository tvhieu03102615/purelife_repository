import React , {Component} from 'react';
import Progress from '../component/Progress';
import {BoneOptions, Allergies} from '../component/BoneOptions';

import { Link } from "react-router-dom";

class AccountSetup extends Component{
  render(){
    return(
      <div id = "account-set-up">
        <h1 class="text-center">Shipping information</h1>
        <section class="get-info section-padding" id="account-set-up-1">
          <div class="container">
            <form action="">
              <div class="row">
                <div class="col-md-6">
                  <input type="text"/><br/>
                  <label for="">First name</label>
                </div>
                <div class="col-md-6"> 
                  <input type="text"/><br/>
                  <label for="">Last name</label>
                </div>
              </div>
              <input type="text"/><br/>
              <label for="">Delivery address 1</label><br/>
              <input type="text"/><br/>
              <label for="">Delivery address 2</label><br/>
              <div class="info-bot">
                <div class="info-bot__1">
                  <input type="text"/><br/>
                  <label for="">City</label>
                </div>
                <div class="info-bot__1">
                  <select id="state" name="">
                    <option value="">CA</option>
                    <option value="">CA</option>
                    <option value="">CA</option>
                  </select><br/>
                  <label for=""> stage</label>
                </div>
                <div class="info-bot__1">
                  <input type="text"/><br/>
                  <label for="">Zip  </label>
                </div>
              </div>
              <button class="btn-default btn-res" type="submit"><i class="fas fa-shopping-cart"> </i>&nbsp;Checkout</button>
            </form>
          </div>
        </section>
      </div>
    );
  }
}
export default AccountSetup;