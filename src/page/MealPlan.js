import React , {Component} from 'react';
import Progress from '../component/Progress';
import {MealPlanPrice, MealPlanCalculate, MealPlanOption} from '../component/MealPlanPrice';
import { Link } from "react-router-dom";

class MealPlan extends Component{  constructor(props){
  super(props);
    this.state = {
      process: {
        index: 2,
      },
      status: 0,
      name : "NaLa",
    };
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }

  handleChangeStatus(stt){
    this.setState({
      status : stt,
    });
  }

  render(){
    return(
      <div id="meal-plan-st1">
        <Progress attr={this.state.process} />
        <MealPlanPrice onChangeStatus={this.handleChangeStatus} status={this.state.status}/>

        <MealPlanOption stt={this.state.status} index="0" head = {"how much food does "+this.state.name+" need?"} content1="Reccomended, best value. 4 week worth of pre-measured frozen meals" content2="Reccomended, best value. 4 week worth of pre-measured frozen meals" title1="- 4 Week Supply -" title2="- 2 Week Supply -" />
        <MealPlanOption stt={this.state.status} index="1" head = "Select your proteins" content1="Variety of different meats included Turkey, Chicken, Beef, and Pork" content2="Reccomended, best value. 4 week worth of pre-measured frozen meals" title1="- Essentials -" title2="- Indulgence -"/>
        <MealPlanOption stt={this.state.status} index="2" head = "What kind of plan would you like?" content1="Reccomended, best value. 4 week worth of pre-measured frozen meals" content2="Reccomended, best value. 4 week worth of pre-measured frozen meals" title1="- Recurring -" title2="- One time -"/>

        <MealPlanCalculate onChangeStatus={this.handleChangeStatus} status={this.state.status}/>
      </div>
    );
  }
}

export default MealPlan 