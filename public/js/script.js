(function($) {
 $.fn.getNumbByPx = function(css) {
  var Css = $(this).css(css);
  var length = Css.length;
  var numb = Number(Css.slice(0, (length - 2)));
  return numb;
 },
 $.fn.disable = function() {
   return this.each(function() {
     if ($(this).hasClass('active')) {
       $(this).removeClass('active')
     }
     $(this).addClass('disable');
   });
 },
 $.fn.active = function(s) {
   return this.each(function() {
     if ($(this).hasClass('disable')) {
       $(this).removeClass('disable');
     }
     if (s) {
       $(this).addClass('active');
     } else {
       $(this).removeClass("active");
     }
   });
 }
 $.fn.scale = function(x) {
   if(!$(this).filter(':visible').length && x!=1)return $(this);
   if(!$(this).parent().hasClass('scaleContainer')){
     $(this).wrap($('<div class="scaleContainer">').css('position','relative'));
     $(this).data({
       'originalWidth':$(window).width(),
       'originalHeight':$(window).height()
     });
   }
   $(this).css({
     'transform': 'scale('+x+')',
     '-ms-transform': 'scale('+x+')',
     '-moz-transform': 'scale('+x+')',
     '-webkit-transform': 'scale('+x+')',
     'transform-origin': '0 0',
     '-ms-transform-origin': '0 0',
     '-moz-transform-origin': '0 0',
     '-webkit-transform-origin': '0 0',
   });
   if(x==1){
     $(this).unwrap().css('position','static');
   }
   else{
     $(this).parent()
       .width($(this).data('originalWidth'))
       .height($(this).data('originalHeight'));
   }
   return $(this);
 };
}(jQuery));
//////////////////////////////////////////
"use strict";
jQuery(document).ready(function($){
  // Window on load
  $(window).on('load', function(){
    /**
     * slide
     */
    $('.slide__owl').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      // navContainer: true,
      navText:["&lsaquo;", "&rsaquo;"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
    });
  });
  // end slide
  $(document).click(function (e) {
    $(".popup-register").disable();
    $(".popup-sigin").disable();
  });

  if($('.btn-register').length !=0){
    $(".btn-register").click(function (e) {
      if (!e)
       e=window.event;
      //IE9 & Other Browsers
      if (e.stopPropagation) {
       e.stopPropagation();
      }
      //IE8 and Lower
      else {
       e.cancelBubble = true
      }
      $(".popup-sigin").disable();
      $(".popup-register").active(1);
    });
  }

  if($('.btn-login').length !=0){
    $(".btn-login").click(function (e) {
      if (!e)
       e=window.event;
      //IE9 & Other Browsers
      if (e.stopPropagation) {
       e.stopPropagation();
      }
      //IE8 and Lower
      else {
       e.cancelBubble = true
      }
      $(".popup-register").disable();
      $(".popup-sigin").active(1);
    });
  }
  $("#form-login, #form-register").click(function (e) {
    if (!e)
     e=window.event;
    //IE9 & Other Browsers
    if (e.stopPropagation) {
     e.stopPropagation();
    }
    //IE8 and Lower
    else {
     e.cancelBubble = true
    }
  });

  $(document).scroll(function (e) {
    if($(this).scrollTop()>=100){
      $("#header").addClass("scroll")
    }else{
      $("#header").removeClass("scroll")
    }
  });
  $(document).scroll(function (e) {
    if($(this).scrollTop()>=100){
      $("#header").addClass("scroll")
    }else{
      $("#header").removeClass("scroll")
    }
  });

  $(".hide").click(function (e) {
    if($(".action-user-site").hasClass("active")){
      $(".action-user-site").removeClass("active");
      $(this).children("i").addClass("fa-user");
      $(this).children("i").removeClass("fa-window-close");
    }
    else{
      $(".action-user-site").addClass("active");
      $(this).children("i").removeClass("fa-user");
      $(this).children("i").addClass("fa-window-close");
    }
    //
  });
  $(".option-st1-1__child").click(function (e) {
    $(".option-st1-1__child").removeClass("active");
    $(this).active(1);
    // $(this).closest("col-md-6").
    $(this).children("input").prop("checked", true);
  });
});
